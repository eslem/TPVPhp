<HTML>
<HEAD>
  <TITLE>PhP Form Example</TITLE>
</HEAD>
<BODY>

<?php
// If form is submitted with all required data then show the form
// else show error page
empty($Formulario) ?
	  ShowForm() :
	  ShowError();
exit;
?>

<?php

function ShowError () {
	echo "<html><head><title>Results</title></head><body><table width=100% height=50%><tr><td><p><h2><center>Compruebe que todos los datos del formulario son correctos!!</center></h2></p></td></tr></table></body></html>\n";
} # End of function ShowError

function ShowForm () {
// Posted data
global $HTTP_POST_VARS;

// Valores constantes del comercio
$url_tpvv='https://sis-t.sermepa.es:25443/sis/realizarPago';
$clave='qwertyasdf0123456789';
$name='Comercio Pruebas';
$code='999008881';
$terminal='4';
$order=date('ymdHis');
$amount='25';
$currency='978';
$transactionType='0';
$urlMerchant='http://sis-d5.sermepa.es/sis/pruebaCom.jsp';
$producto='Zapatos';

// Now, print the HTML script
echo "<html><head><title>Comercio Simulador</title></head>
<script language=JavaScript>
function calc() { 
vent=window.open('','tpv','width=725,height=600,scrollbars=no,resizable=yes,status=yes,menubar=no,location=no');
document.forms[0].submit();}
</script>
<body bgcolor=white>
<form name=compra action=$url_tpvv method=post target=tpv>
<pre>
<table>
<tr><td>
<h2>Comercio de prueba.</h2>
</td></tr><tr><td>
Comercio: <font color=blue>$name</font>
</td></tr><tr><td>
FUC: <font color=blue>$code</font>
</td></tr><tr><td>
Terminal: <font color=blue>$terminal</font>
</td></tr><tr><td>
Pedido: <font color=blue>$order</font>
</td></tr><tr><td>
Producto: <font color=blue>$producto</font>
</td></tr><tr><td>
Importe: <font color=blue>$amount</font>
</td></tr><tr><td>
Tipo de Operacion: <font color=blue>$transactionType (Autorización)</font>
</td></tr><tr><td>
URL del comercio: <font color=blue>$urlMerchant</font>
</td></tr><tr><td>";

// Currency strings 
if ($currency == "978") {
	echo "Moneda: <font color=blue>Euros</font>";
}

echo "</td>
</tr><tr><td>
<input type=hidden name=Ds_Merchant_Amount value='$amount'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_Currency value='$currency'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_Order  value='$order'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_MerchantCode value='$code'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_Terminal value='$terminal'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_TransactionType value='$transactionType'>
</td></tr><tr><td>
<input type=hidden name=Ds_Merchant_MerchantURL value='$urlMerchant'>
</td></tr><tr><td>";

// Compute hash to sign form data
$message = $amount.$order.$code.$currency.$transactionType.$urlMerchant.$clave;
// echo "<input type=hidden name=message value='$message'>";
$signature = strtoupper(sha_1($message));

echo "<input type=hidden name=Ds_Merchant_MerchantSignature value='$signature'>

</td></tr>
</table>
<center><a href='javascript:calc()'><img src='/tpvirtual.jpg' border=0 ALT='TPV Virtual'></a></center>
</pre>
</form>										  
</body></html>";
} # End of function ShowForm


?>

<?php

/*
** Date modified: 1st October 2004 20:09 GMT
*
** PHP implementation of the Secure Hash Algorithm ( SHA-1 )
*
** This code is available under the GNU Lesser General Public License:
** http://www.gnu.org/licenses/lgpl.txt
*
** Based on the PHP implementation by Marcus Campbell
** http://www.tecknik.net/sha-1/
*
** This is a slightly modified version by me Jerome Clarke ( sinatosk@gmail.com )
** because I feel more comfortable with this
*/

function sha1_str2blks_SHA1($str)
{
   $strlen_str = strlen($str);
   
   $nblk = (($strlen_str + 8) >> 6) + 1;
   
   for ($i=0; $i < $nblk * 16; $i++) $blks[$i] = 0;
   
   for ($i=0; $i < $strlen_str; $i++)
   {
       $blks[$i >> 2] |= ord(substr($str, $i, 1)) << (24 - ($i % 4) * 8);
   }
   
   $blks[$i >> 2] |= 0x80 << (24 - ($i % 4) * 8);
   $blks[$nblk * 16 - 1] = $strlen_str * 8;
   
   return $blks;
}

function sha1_safe_add($x, $y)
{
   $lsw = ($x & 0xFFFF) + ($y & 0xFFFF);
   $msw = ($x >> 16) + ($y >> 16) + ($lsw >> 16);
   
   return ($msw << 16) | ($lsw & 0xFFFF);
}

function sha1_rol($num, $cnt)
{
   return ($num << $cnt) | sha1_zeroFill($num, 32 - $cnt);    
}

function sha1_zeroFill($a, $b)
{
   $bin = decbin($a);
   
   $strlen_bin = strlen($bin);
   
   $bin = $strlen_bin < $b ? 0 : substr($bin, 0, $strlen_bin - $b);
   
   for ($i=0; $i < $b; $i++) $bin = '0'.$bin;
   
   return bindec($bin);
}

function sha1_ft($t, $b, $c, $d)
{
   if ($t < 20) return ($b & $c) | ((~$b) & $d);
   if ($t < 40) return $b ^ $c ^ $d;
   if ($t < 60) return ($b & $c) | ($b & $d) | ($c & $d);
   
   return $b ^ $c ^ $d;
}

function sha1_kt($t)
{
   if ($t < 20) return 1518500249;
   if ($t < 40) return 1859775393;
   if ($t < 60) return -1894007588;
   
   return -899497514;
}

function sha_1($str, $raw_output=FALSE)
{
   if ( $raw_output === TRUE ) return pack('H*', sha_1($str, FALSE));
   
   $x = sha1_str2blks_SHA1($str);
   $a =  1732584193;
   $b = -271733879;
   $c = -1732584194;
   $d =  271733878;
   $e = -1009589776;
   
   $x_count = count($x);
   
   for ($i = 0; $i < $x_count; $i += 16)
   {
       $olda = $a;
       $oldb = $b;
       $oldc = $c;
       $oldd = $d;
       $olde = $e;
       
       for ($j = 0; $j < 80; $j++)
       {
           $w[$j] = ($j < 16) ? $x[$i + $j] : sha1_rol($w[$j - 3] ^ $w[$j - 8] ^ $w[$j - 14] ^ $w[$j - 16], 1);
           
           $t = sha1_safe_add(sha1_safe_add(sha1_rol($a, 5), sha1_ft($j, $b, $c, $d)), sha1_safe_add(sha1_safe_add($e, $w[$j]), sha1_kt($j)));
           $e = $d;
           $d = $c;
           $c = sha1_rol($b, 30);
           $b = $a;
           $a = $t;
       }
       
       $a = sha1_safe_add($a, $olda);
       $b = sha1_safe_add($b, $oldb);
       $c = sha1_safe_add($c, $oldc);
       $d = sha1_safe_add($d, $oldd);
       $e = sha1_safe_add($e, $olde);
   }
   
   return sprintf('%08x%08x%08x%08x%08x', $a, $b, $c, $d, $e);
}
?> 
